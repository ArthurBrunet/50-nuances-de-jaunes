<?php

class Form
{
    protected $fileIni;
    protected $dataIni;
    protected $form;
    protected $selfClosingTag = array("input", "img", "br");

    public function __construct($form)
    {
        $this->form = "form" . $form;
        $this->fileIni = "./conf/form" . $form . ".ini";

        if (is_file($this->fileIni))
            $this->dataIni = parse_ini_file($this->fileIni, true);

        else
            return false;
    }

    public function getFormHtml()
    {
        $htmlContent = "<form ";

        foreach ($this->dataIni as $key => $value) {
            if ($key !== "form") {

                $keySubString = substr($key, 0, -2);

                $htmlContent .= "<$keySubString";

                foreach ($this->dataIni[$key] as $key2 => $value2) {
                    if ($key2 !== "displayedText")
                        $htmlContent .= " $key2=\"$value2\"";
                }

                if (in_array($keySubString, $this->selfClosingTag))
                    $htmlContent .= " />";

                else {
                    $htmlContent .= ">";
                    $htmlContent .= $value['displayedText'] ?? "";
                    $htmlContent .= "</$keySubString>";
                }
            }

            else {
                foreach ($this->dataIni[$key] as $key2 => $value2) {
                        $htmlContent .= " $key2=\"$value2\"";
                }
                $htmlContent .= ">";
            }
        }

        $htmlContent .= "</form>";

        return $htmlContent;
    }

    public function checkForm()
    {
        if (isset($_POST) && isset($_POST[$this->form])) {
            foreach ($this->dataIni as $key => $value) {
                if ($key === "input" || $key === "textarea" || $key === "select") {

                }

            }
        }

        else
            return false;

    }
}
